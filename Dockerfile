FROM ubuntu:16.04

ENV CONDA_DIR="/opt/conda"
ENV PATH="$CONDA_DIR/bin:$PATH"
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Install conda
RUN CONDA_VERSION="4.6.14" && \
    CONDA_MD5_CHECKSUM="718259965f234088d785cad1fbd7de03" && \
    \
    apt update && \
    apt install --no-install-recommends -y wget tar ca-certificates bzip2 curl git ssh-client && \
    apt clean &&\
    rm -rf /var/lib/apt/lists/* && \
    \
    mkdir -p "$CONDA_DIR" && \
    wget "http://repo.continuum.io/miniconda/Miniconda3-${CONDA_VERSION}-Linux-x86_64.sh" -O miniconda.sh && \
    echo "$CONDA_MD5_CHECKSUM  miniconda.sh" | md5sum -c && \
    bash miniconda.sh -f -b -p "$CONDA_DIR" && \
    rm miniconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc && \
    \
    conda update --all --yes && \
    conda config --set auto_update_conda False && \
    conda init bash

# CUDA Runtime
ENV CUDA_VERSION=8.0.61
ENV CUDA_PKG_VERSION=8-0=$CUDA_VERSION-1
RUN apt-get update && apt-get install -y --no-install-recommends ca-certificates apt-transport-https gnupg-curl && \
    rm -rf /var/lib/apt/lists/* && \
    NVIDIA_GPGKEY_SUM=d1be581509378368edeec8c1eb2958702feedf3bc3d17011adbf24efacce4ab5 && \
    NVIDIA_GPGKEY_FPR=ae09fe4bbd223a84b2ccfce3f60f4b3d7fa2af80 && \
    \
    apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub && \
    apt-key adv --export --no-emit-version -a $NVIDIA_GPGKEY_FPR | tail -n +5 > cudasign.pub && \
    echo "$NVIDIA_GPGKEY_SUM  cudasign.pub" | sha256sum -c --strict - && rm cudasign.pub  && \
    echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64 /" > /etc/apt/sources.list.d/cuda.list  && \
    echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1604/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list && \
    \
    apt-get update && apt-get install -y --no-install-recommends \
          cuda-nvrtc-$CUDA_PKG_VERSION \
          cuda-nvgraph-$CUDA_PKG_VERSION \
          cuda-cusolver-$CUDA_PKG_VERSION \
          cuda-cublas-8-0=8.0.61.2-1 \
          cuda-cufft-$CUDA_PKG_VERSION \
          cuda-curand-$CUDA_PKG_VERSION \
          cuda-cusparse-$CUDA_PKG_VERSION \
          cuda-npp-$CUDA_PKG_VERSION \
          cuda-cudart-$CUDA_PKG_VERSION && \
    \
    ln -s cuda-8.0 /usr/local/cuda && \
    rm -rf /var/lib/apt/lists/* && \
    \ 
    echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf && \
    apt clean &&\
    rm -rf /var/lib/apt/lists/*

# CUDA Devel
RUN apt-get update && apt-get install -y --no-install-recommends \
    cuda-core-$CUDA_PKG_VERSION \
    cuda-misc-headers-$CUDA_PKG_VERSION \
    cuda-command-line-tools-$CUDA_PKG_VERSION \
    cuda-nvrtc-dev-$CUDA_PKG_VERSION \
    cuda-nvml-dev-$CUDA_PKG_VERSION \
    cuda-nvgraph-dev-$CUDA_PKG_VERSION \
    cuda-cusolver-dev-$CUDA_PKG_VERSION \
    cuda-cublas-dev-8-0=8.0.61.2-1 \
    cuda-cufft-dev-$CUDA_PKG_VERSION \
    cuda-curand-dev-$CUDA_PKG_VERSION \
    cuda-cusparse-dev-$CUDA_PKG_VERSION \
    cuda-npp-dev-$CUDA_PKG_VERSION \
    cuda-cudart-dev-$CUDA_PKG_VERSION \
    cuda-driver-dev-$CUDA_PKG_VERSION && \
    \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

# CUDA Hoomd
ENV SOFTWARE_ROOT=/opt/hoomd
ENV PYTHONPATH=$PYTHONPATH:${SOFTWARE_ROOT}/lib/python
RUN apt-get update && apt-get install -y --no-install-recommends git cmake && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip --no-cache-dir install numpy && \
    export HOOMD_TAG=v2.8.1-dybond && \
    git clone --recursive https://mikemhenry@bitbucket.org/cmelab/hoomd_blue.git && \
    cd hoomd_blue && \
    git checkout $HOOMD_TAG && \
    mkdir build && \
    cd build && \
    export CXX="$(command -v g++)" && \
    export CC="$(command -v gcc)" && \
    cmake ../ -DCMAKE_INSTALL_PREFIX=${SOFTWARE_ROOT}/lib/python \
              -DENABLE_CUDA=ON \
              -DDISABLE_SQLITE=ON \
              -DSINGLE_PRECISION=ON && \
    make -j$(nproc) && \
    make install

# mount points for filesystems on clusters
RUN mkdir -p /nfs \
    mkdir -p /oasis \
    mkdir -p /scratch \
    mkdir -p /work \
    mkdir -p /projects \
    mkdir -p /home1
